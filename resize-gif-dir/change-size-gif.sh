#! /bin/bash

GREEN='\033[0;32m'
RED='\033[0;31m'
WHITE='\033[0;37m'
RESET='\033[0m'


read -p "Enter existing directory : " input


if [ -z "$input" ];  then
    echo "Argument, must be an existing directory"
    exit 1
fi

read -p "Enter Original Size : " orgsz


if [ -z "$orgsz" ];  then
	echo "Argument, must be Input Original Size  000x000"
	exit 1
fi

read -p "Enter Final Size : " finalsz

if [ -z "$finalsz" ];  then
	echo "Argument, must be Input Final Size 000x000"
	exit 1
fi
 
echo ""

gifs=$(find $input -type f -iname '*.gif')


for p in $gifs; do
    echo "$p"   
	
	 
	 convert -size "#$orgsz" $p -resize "#$finalsz" $p  | pv  -t
	 echo ""
done


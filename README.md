### NAME
       convert-to-gif.sh

### LOCATION
       /usr/local/bin/convert-to-gif.sh

### HOW IT WORKS
       It uses FFmpeg to convert a Mp4 to a Gif.

       $ change-to-gif.sh <mp4 file> <frames> (default 28)

       [Enter new name]:

       Done.

### DESCRIPTION
       Mp4 to gif converter, using ffmpeg.

       Set frame rate ...

### OPTIONS
       Utility to convert video files to GIFs using ffmpeg

       Usage: convert-to-gif.sh <video-file-path>

       To skip frames: convert-to-gif.sh <video-file-path> <time-in-seconds>

### EXAMPLE
       $ convert-to-gif.sh video.mp4 25

### COMMENDS
       When a .mp4 file is not set, it will ask for it.

       When frame rate is not set, it will ask for it.

       Control+C to stop the process.

       Or it will use frame rate (28).

### BUGS
       No known bugs.

### AUTHOR
       Fork ::: Original unknown

       musq (luftballon@tutanota.com)

---


![Schermafdruk_2021-11-26_17-52-11](/uploads/74a50d722d3eb99f52e6ef126a914cf5/Schermafdruk_2021-11-26_17-52-11.png)

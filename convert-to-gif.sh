#!/usr/bin/env bash

# Utility to convert video files to GIFs using ffmpeg
#
# Usage: convert-to-gif.sh <video-file-path>
# To skip frames: convert-to-gif.sh <video-file-path> <time-in-seconds>
# Example:
# convert-to-gif.sh video.mp4 

read -p "Enter input file name.mp4 : " input

if [ -z "$input" ];  then
    echo "No video file specified"
    exit 1
fi

read -p "Enter frames : " frame

if [ -z "$frame" ];  then
	echo ""
	echo "###############################################################################"
    echo "                              No Frames set!!!"
    echo ""
    echo "  			Using now the default of (22) "
    echo ""
    echo "  			EXAMPLE : $ convert-to-gif.sh 28 "
    echo ""
    echo ""
    echo "						 Stop process with Control-c"
    echo "###############################################################################"
    echo ""
	
fi

read -p "Enter Name for Gif : " newname ; 

	
if [ -z "$newname" ];
	then 
     echo "No new filename specified"
     exit 1
fi

sleep 1 ;

# get everything after last /
video=${1##*/}
# remove everything after .
# newname=${video%.*}

echo -e "$(tput bold)Getting video dimensions $(tput sgr0)"
# Get video dimensions
dimensions=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$input")

echo -e "$(tput bold)Generating Palette $(tput sgr0)"
# Generate palette
ffmpeg -i "$input" -vf "fps=22,scale=${dimensions%x*}:-1:flags=lanczos,palettegen" "$newname".png

echo -e "$(tput bold)Converting Video to GIF $(tput sgr0)"

if [[ "$frame" ]]; then
    ffmpeg -t "$frame" -i "$input" -i "$newname".png -filter_complex "fps=22,scale=${dimensions%x*}:-1:flags=lanczos[x];[x][1:v]paletteuse" "$newname".gif
else
    ffmpeg -i "$input" -i "$newname".png -filter_complex "fps=22,scale=${dimensions%x*}:-1:flags=lanczos[x];[x][1:v]paletteuse" "$newname".gif
fi

echo -e "Removing palette"
